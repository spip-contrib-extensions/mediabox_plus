;
(function($) {
	/*
	 *  Implémentation de mediabox basée sur featherlight
	 *  https://github.com/noelboss/featherlight
	 */
	 jQuery.fn.extend({

	 	mediabox: function(options) {

	 		var cfg = $.extend({}, options);

			var href = cfg.href || ""; // content
			var galerie = !!cfg.slideshow || !!cfg.rel || false;

			if (!!cfg.className) {
				cfg.variant = cfg.className;
			}
			// routage des callbacks
			if (!!cfg.onOpen) {
				cfg.beforeContent = cfg.onOpen;
			}
			if (!!cfg.onShow) {
				cfg.afterOpen = cfg.onShow;
			}
			if (!!cfg.onClose) {
				cfg.afterClose = cfg.onClose;
			}
			// clic sur background déclenche fermeture
			cfg.closeOnClick = (cfg.overlayClose !== false) ? 'background' : false;

			if (this === jQuery.fn) {
				return (galerie) ? $.featherlightGallery(href, cfg) : $.featherlight(href, cfg);
			} else {
				var b = typeof(mediabox_settings) == 'object' ? mediabox_settings : {};
				if (b.ns) {
					this.find('[data-'+b.ns+'-type]').each(function(i,e) {
						var $e = $(e);
						var d = $e.attr('data-'+b.ns+'-type');
						$e.removeAttr('data-'+b.ns+'-type').attr('data-featherlight-type',d);
					});
				}
				return (galerie) ? this.featherlightGallery(cfg) : this.featherlight(cfg);
			}

		},

		mediaboxClose: function() {
			return $.featherlight.close();
		}

	});

	 var initConfig = function() {

	 	/* featherlight Settings */
			// namespace:      'featherlight',        /* Name of the events and css class prefix */
			// targetAttr:     'data-featherlight',   /* Attribute of the triggered element that contains the selector to the lightbox content */
			// variant:        null,                  /* Class that will be added to change look of the lightbox */
			// resetCss:       false,                 /* Reset all css */
			// background:     null,                  /* Custom DOM for the background, wrapper and the closebutton */
			// openTrigger:    'click',               /* Event that triggers the lightbox */
			// closeTrigger:   'click',               /* Event that triggers the closing of the lightbox */
			// filter:         null,                  /* Selector to filter events. Think $(...).on('click', filter, eventHandler) */
			// root:           'body',                /* Where to append featherlights */
			// openSpeed:      250,                   /* Duration of opening animation */
			// closeSpeed:     250,                   /* Duration of closing animation */
			// closeOnClick:   'background',          /* Close lightbox on click ('background', 'anywhere' or false) */
			// closeOnEsc:     true,                  /* Close lightbox when pressing esc */
			// closeIcon:      '&#10005;',            /* Close icon */
			// loading:        '',                    /* Content to show while initial content is loading */
			// persist:        false,                 /* If set, the content will persist and will be shown again when opened again. 'shared' is a special value when binding multiple elements for them to share the same content */
			// otherClose:     null,                  /* Selector for alternate close buttons (e.g. "a.close") */
			// beforeOpen:     $.noop,                /* Called before open. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
			// beforeContent:  $.noop,                /* Called when content is loaded. Gets event as parameter, this contains all data */
			// beforeClose:    $.noop,                /* Called before close. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
			// afterOpen:      $.noop,                /* Called after open. Gets event as parameter, this contains all data */
			// afterContent:   $.noop,                /* Called after content is ready and has been set. Gets event as parameter, this contains all data */
			// afterClose:     $.noop,                /* Called after close. Gets event as parameter, this contains all data */
			// onKeyUp:        $.noop,                /* Called on key up for the frontmost featherlight */
			// onResize:       $.noop,                /* Called after new content and when a window is resized */
			// type:           null,                  /* Specify type of lightbox. If unset, it will check for the targetAttrs value. */
			// contentFilters: ['jquery', 'image', 'html', 'ajax', 'iframe', 'text'], /* List of content filters to use to determine the content */
			// jquery/image/html/ajax/iframe/text: undefined /* Specify content type and data */

			// recuperer les préférences de l'utilisateur
			var b = typeof(mediabox_settings) == 'object' ? mediabox_settings : {};

			// CONFIGURATION GENERALE
			core_defaults = {
				namespace: 'featherlight',
				targetAttr: 'data-href-popin',
				variant: 'box_mediabox box_modalbox',
				closeIcon: '✖',
				loading: '<span class="box-loading"></span>',
				openSpeed: 0,
				closeSpeed: 250,
				//persist: 'shared',
				beforeOpen: function(e) {
					var $i = this.$instance;
					jQuery('.' + b.ns).not($i).addClass('unfocused');
					$i.addClass('opening');
					//$i.addClass(b.ns + '--' +  this.type);
				},
				afterOpen: function(e) {
					var $i = this.$instance;
					$i.find('button[class$=close]').attr('aria-label', b.str_close);
					$i.find('span[class$=-previous]').attr('title', b.str_prev);
					$i.find('span[class$=-next]').attr('title', b.str_next);
					$i.removeClass('opening unfocused').addClass('open');
				},
				beforeContent: function(e) {
					var $i = this.$instance;
					if (this.type === 'image') {
						$i.addClass('fullscreen');
					}
				},
				afterContent: function(e) {
					var $i = this.$instance;
					if (this.type === 'image') {
						// caption
						var title = this.$currentTarget.attr('title') || false;
						var $caption = jQuery('.featherlight .featherlight-title').eq(0);
						if (title) {
							if ($caption.length) {
								$caption.html(title);
							}
							else {
								var ajout = 
									jQuery('<div/>')
									  .addClass('featherlight-title')
									  .html(title)
									  .appendTo($i.children().eq(0));
							}
						}
						else {
							$caption.remove();
						}
					}
					// ajaxreload
					if ((this.type == 'ajax') && (typeof(jQuery.spip.triggerAjaxLoad) != 'undefined')) {
						jQuery.spip.triggerAjaxLoad($i);
					}			        			
				},
				beforeClose: function(e) {
					var $i = this.$instance;
					$i.removeClass('open').addClass('closing');
				},
				afterClose: function(e) {
				},
			}
			// CONFIGURATION GALERIE
			galerie_defaults = {
				previousIcon: '❮',
				nextIcon: '❯',
				galleryFadeIn: 100,
				galleryFadeOut: 300,
				//autoBind: '[rel="galerieauto"]'
			};

			if (typeof($.featherlight) == 'function') {
				$.featherlight.defaults = $.extend($.featherlight.defaults, core_defaults);
			}
			if (typeof($.featherlightGallery) == 'function') {
				$.featherlightGallery.defaults = $.extend($.featherlightGallery.defaults, galerie_defaults);
			}

		}
		// on écrase la config juste une fois
		initConfig();

	})(jQuery);