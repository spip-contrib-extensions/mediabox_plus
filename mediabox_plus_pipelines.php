<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function mediabox_plus_mediabox_config($config) {

	$config['_libs']['featherlight'] = [
		'nom' => 'Featherlight',
		'css' => [],
		'js' => [
			'lib/featherlight/featherlight.min.js',
			'lib/featherlight/featherlight.gallery.min.js',
			'featherlight/js/featherlight.mediabox.js',
		]
	];

	if (empty($config['featherlight'])) {
		$config['featherlight'] = [];
	}

	$config['featherlight'] = array_merge(
		[
			'couleur' => '#222222',
			'opacite' => '0.9',
		]
		, $config['featherlight']
	);

	// pas de skin, une seule css dispo
	$box_skin = 'plume';
	$config['_libs']['featherlight']['css'][] = "lib/featherlight/featherlight.min.css";
	$config['_libs']['featherlight']['css'][] = "lib/featherlight/featherlight.gallery.min.css";
	$config['_libs']['featherlight']['css'][] = "featherlight/css/featherlight.mediabox.css";

	return $config;
}

function filtre_couleur_hex_to_dec_dist($couleur) {
	include_spip('inc/filtres_images_lib_mini');
	return _couleur_hex_to_dec($couleur);
}